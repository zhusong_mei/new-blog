# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'b1d1b51eb411e8cb15afabc89d8856c11295c775ea07a4a28ab67d2277366149345d0e4a2301d8dd294003066c6704dfc4b6748c780a4e5bf21487b0bee1cd32'
